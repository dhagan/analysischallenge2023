#include <main.hxx>

enum sample{ s1500_300, s1500_500, s1000_300, s1000_500, LAST };

static std::vector< std::string > sigs = { "SqSq_Hplus_1500_300", "SqSq_Hplus_1500_500", "SqSq_Hplus_1000_300", "SqSq_Hplus_1000_500" };
static std::unordered_map< sample, std::string > signals = { { s1500_300, "SqSq_Hplus_1500_300" }, { s1500_500, "SqSq_Hplus_1500_500" },
                                                             { s1000_300, "SqSq_Hplus_1000_300" }, { s1000_500, "SqSq_Hplus_1000_500" } };

static std::vector< std::string > trigger_cuts = { "passTrig_MET>=1.0", "passTrig_1tau>=1.0", "passTrig_1gamma>=1.0 " };


static std::string data_chain_path = std::string( std::getenv( "DATA_PATH") ) +  "/data.root/ntuple_THEORY";
static std::string signal_path_1 = std::string( std::getenv( "DATA_PATH") );
static std::string signal_path_2 = ".root/ntuple_THEORY";

// perform a single results check, checks all 4 signal files, calculates significance and contamination;
void trigger( std::string & trigger_cut ){

  
  TChain * data_chain = new TChain;
  data_chain->Add( data_chain_path.c_str() );
  ROOT::RDataFrame data( *data_chain );
  float total_data = *data.Count();
  float pass_data = *data.Filter( trigger_cut ).Count();

  std::cout << trigger_cut << std::endl;

  std::cout << "Data \t Ratio:" << pass_data/total_data << std::endl;
  //std::cout << "\t Passed:" << pass_data << std::endl;
  //std::cout << "\t Total:" << total_data << std::endl;
  //std::cout << "\t Ratio:" << pass_data/total_data << std::endl;
  
  for ( int sample_idx = s1500_300; sample_idx < LAST; sample_idx++ ){

    sample sampletype = static_cast< sample >( sample_idx );
    std::string signal_chain_path = signal_path_1 + signals[sampletype] + signal_path_2;

    TChain * signal_chain = new TChain;
    signal_chain->Add( signal_chain_path.c_str() );
    ROOT::RDataFrame signal( *signal_chain );

    float total_signal = *signal.Count();
    float pass_signal = *signal.Filter( trigger_cut ).Count();

    std::cout << signals.at(sampletype) << "\t Ratio:" <<  pass_signal/total_signal << std::endl;
    //std::cout << "\t Passed:" << pass_signal << std::endl;
    //std::cout << "\t Total:" <<  total_signal << std::endl;
    //std::cout << "\t Ratio:" <<  pass_signal/total_signal << std::endl;

  }
}

// entry point
int main(){

  // RDataFrame behviour
  ROOT::EnableImplicitMT( 6 );
  ROOT::EnableThreadSafety(); // for a future optimisation;
  
  for ( std::string & trigger_cut : trigger_cuts ){
    trigger( trigger_cut );
  }

}
    


