#include <main.hxx>

// statics and enums
enum region{ A, B, C, D };

enum sample{ s1500_300, s1500_500, s1000_300, s1000_500, LAST };

struct chains{
  TChain * data_chain;
  std::unordered_map< sample, TChain * > signal_chains;
};

struct frames{
  TChain * data_frame;
  std::unordered_map< sample, ROOT::RDataFrame > signal_frames;
};

struct result{
  std::string cut;
  std::unordered_map< sample, double > significance;
  std::unordered_map< sample, double > contamination;
};

static std::map<std::string, std::vector<int>> ranges = { { "bjet1pT>=", {50, 125, 200} }, { "HT>=", { 400, 600, 800} },
                                                          { "jet1pT>=", { 100, 200, 300 } }, { "jet2M>=", { 10, 17, 25 } }, 
                                                          { "MET<", { 200, 400, 600 } } };

static std::unordered_map< region, std::string > regions = { { A, "mjj < 150 && mtaugamma < 150" }, { B, "mjj > 150 && mtaugamma < 150" }, 
                                                             { C, "mjj < 150 && mtaugamma > 150" }, { D, "mjj > 150 && mtaugamma > 150" } };

static std::vector< std::string > sigs = { "SqSq_Hplus_1500_300", "SqSq_Hplus_1500_500", "SqSq_Hplus_1000_300", "SqSq_Hplus_1000_500" };

static std::unordered_map< sample, std::string > signals = { { s1500_300, "SqSq_Hplus_1500_300" }, { s1500_500, "SqSq_Hplus_1500_500" },
                                                             { s1000_300, "SqSq_Hplus_1000_300" }, { s1000_500, "SqSq_Hplus_1000_500" } };

static std::string data_chain = std::string( std::getenv( "DATA_PATH") ) +  "/data.root/ntuple_THEORY";
static std::string sign_path_1 = std::string( std::getenv( "DATA_PATH") );
static std::string sign_path_2 = ".root/ntuple_THEORY";

// create the combinations for the grid scan
std::vector< std::string > combine( std::stack< std::pair< std::string, std::vector< int >> > ranges ){

  // vector hand-initialised with 3, doing a 3 step grid scan;
  std::vector< std::string > cut_vector = { "", "", "" };

  // pop through ranges stack, create cut combinations
  while ( !ranges.empty() ){

    std::pair< std::string, std::vector<int> > range = ranges.top();
    ranges.pop();

    std::vector<std::string> current_vector = {};
    current_vector.reserve( cut_vector.size() * range.second.size() );

    std::string & range_str = range.first;
  
    for ( std::string & cut : cut_vector ){
      for ( int & value : range.second ){
        current_vector.push_back( cut + range_str + std::to_string( value ) + "&&" );
      }
    }
    cut_vector = current_vector;
  }
  return cut_vector;
}

// perform a single results check, checks all 4 signal files, calculates significance and contamination;
result scan( std::string & cuts, chains & chain ){

  result cut_result;

  ROOT::RDataFrame df_data( *(chain.data_chain) );

  std::string & regA = regions.at( A );
  std::string & regB = regions.at( B );
  std::string & regC = regions.at( C );
  std::string & regD = regions.at( D );

  float A = *df_data.Filter(cuts+regA).Count();
  float B = *df_data.Filter(cuts+regB).Count();
  float C = *df_data.Filter(cuts+regC).Count();
  float Dbkg = (B*C)/A;

  for ( int sample_idx = s1500_300; sample_idx < LAST; sample_idx++ ){

    sample sampletype = static_cast< sample >( sample_idx );
    float Asig = 0, Bsig = 0, Csig = 0, Dsig = 0;

    ROOT::RDataFrame df_sig( *chain.signal_chains.at(sampletype) );
    df_sig.Filter( cuts + regA ).Foreach( [&Asig]( float & weight ){ Asig += weight * 140; }, { "EventWeight" } );
    df_sig.Filter( cuts + regB ).Foreach( [&Bsig]( float & weight ){ Bsig += weight * 140; }, { "EventWeight" } );
    df_sig.Filter( cuts + regC ).Foreach( [&Csig]( float & weight ){ Csig += weight * 140; }, { "EventWeight" } );
    df_sig.Filter( cuts + regD ).Foreach( [&Dsig]( float & weight ){ Dsig += weight * 140; }, { "EventWeight" } );

    cut_result.significance[sampletype] = Dsig/std::sqrt( Dbkg );
    cut_result.contamination[sampletype] = Csig/C;
  }

  cut_result.cut = cuts;
  return cut_result;
}

// a couple of sort booleans for significance and conatmination
bool sort_z( sample sampletype, result & first, result & second ){ 
  return first.significance[sampletype] < second.significance[sampletype];
}
bool sort_c( sample sampletype, result & first, result & second ){ 
  return first.contamination[sampletype] < second.contamination[sampletype];
}

// traverse the results objects, filter out high-conamination
// sort for highest significance and lowest contamination
// dump to file and stdout
void traverse_results( std::vector< result > & results ){
  
  for ( int sample_idx = s1500_300; sample_idx < LAST; sample_idx++ ){

    std::vector< result > filtered_results;
    sample sampletype = static_cast<sample>( sample_idx ); 
    

    std::ofstream outstream;
    std::string output_filename = signals.at(sampletype) + "_results.txt"; 
    outstream.open( output_filename );

    std::copy_if( results.begin(), results.end(), 
                  std::back_inserter( filtered_results ),
                  [&sampletype]( result & results ){ return ( results.contamination.at(sampletype) < 0.1 ); } );
    
    for ( result & fr : filtered_results ){
      outstream << "Cut " << fr.cut << std::endl;
      outstream << "\t Significance:  " << fr.significance.at(sampletype) << std::endl;
      outstream << "\t Contamination: " << fr.contamination.at(sampletype) << std::endl;
    }


    std::vector< result > significance_sorted( filtered_results.size() );
    std::vector< result > contamination_sorted( filtered_results.size() );

    auto significance_sort = [ &sampletype ]( result & first, result & second ){ return sort_z( sampletype, first, second ); };
    std::partial_sort_copy( filtered_results.begin(), filtered_results.end(), significance_sorted.begin(), 
                            significance_sorted.end(), significance_sort );
    auto contamination_sort = [ &sampletype ]( result & first, result & second ){ return sort_z( sampletype, first, second ); };
    std::partial_sort_copy( filtered_results.begin(), filtered_results.end(), contamination_sorted.begin(), 
                            contamination_sorted.end(), contamination_sort );


    float c = contamination_sorted.at( 0 ).contamination.at( sampletype );
    float z = significance_sorted.at( significance_sorted.size()-1 ).significance.at( sampletype );
    std::string & c_cut = contamination_sorted.at( 0 ).cut;
    std::string & z_cut = significance_sorted.at( significance_sorted.size()-1 ).cut;

    std::cout << "Sample: " << signals.at(sampletype) << std::endl;
    std::cout << "\t Best significance: " << z << " " << z_cut << std::endl;
    std::cout << "\t Best contamination: " << c << " " << c_cut << std::endl;

    outstream.close();
  }
}

// entry point
int main(){

  // RDataFrame behviour
  ROOT::EnableImplicitMT( 8 );

  std::stack< std::pair< std::string, std::vector<int> > > range_stack;
  
  // create range stack, produce combinations
  for ( auto range : ranges ){ range_stack.push( std::pair< std::string, std::vector<int>>{ range.first, range.second } );  }
  std::vector<std::string> cuts = combine( range_stack );

  // perform gridscan
  
  chains chain;
  chain.data_chain = new TChain;
  chain.data_chain->Add( data_chain.c_str() );
  for ( int sample_idx = s1500_300; sample_idx < LAST; sample_idx++ ){
    sample sampletype = static_cast< sample >( sample_idx );
    std::string sig_chain = sign_path_1 + signals[sampletype] + sign_path_2;
    chain.signal_chains.insert( std::pair< sample, TChain *>{sampletype, new TChain } );
    chain.signal_chains.at(sampletype)->Add( sig_chain.c_str() );
  }

  std::vector< result > results;
  int cut_number = 0;
  for ( std::string & cut : cuts ){  
    std::cout << "Grid point " << cut_number++ << " - " << cut << std::endl;
    results.push_back( scan( cut, chain ) );
    if ( cut_number >= 10 ){ break; }
  }

  // sift through results
  traverse_results( results );

}
    


