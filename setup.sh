#!/bin/bash

help_func()
{
   echo ""
   echo "Usage: $0 -i"
   echo -i "\tInitial execution, ensures reconfigure"
   return # Exit script after printing help
}

DATA_PATH="$(pwd)/data/"
if [ -n "$DATA_PATH" ]; then
	export DATA_PATH="${DATA_PATH}"
else
    	echo "SET DATA PATH BEFORE SETUP."
	return
fi

initial_flag=
OPTIND=1
while getopts "i" opt
do
   case "$opt" in
      i ) initial_flag=1 ;;
      ? ) help_func ;; # Print helpFunction in case parameter is non-existent
   esac
done


## Fresh shell setup
if [[ -z ${setup} ]]; then


	## Title
	cat ./assets/banner.txt
	echo ""

	## Initialise the submodule
	git submodule update --init --recursive
	
	## Check root and cmake are here
	if ! command -v root &> /dev/null
	then
	    echo "No root install, exiting."
	    return
	fi
	echo -e "$(root --version)"
	
	if ! command -v cmake &> /dev/null
	then
	    echo "No cmake install, exiting."
	    return
	fi
	echo -e $(cmake --version)
	echo ""

fi

mkdir -p build

pushd ./build >> /dev/null
if [ ! -z "$initial_flag" ]; then
	cmake -S .. -B . -DCMAKE_EXPORT_COMPILE_COMMANDS=1
fi
make setup
make install
source environment.sh
popd >> /dev/null
ln -sf ./build/compile_commands.json
export setup=1
echo ""
