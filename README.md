# Submission for the 2023 Susy Challenge
## Neza Ribaric & Alina Isobel Hagan
### Lancaster University

* Initially, run the following command;
```
setupATLAS && lsetup "root 6.28.04-x86_64-centos7-gcc11-opt" && lsetup "views LCG_102b_ATLAS_2 x86_64-centos7-gcc11-opt"
```
* Following this; set the 'DATA\_PATH" variable in the setup.sh script, to point to the directory containing your analysis files.
* Next, use the following for first-time setup;
```
source setup.sh -i
```
* If returning with a fresh shell, or recompiling, simply use;
```
source setup.sh
```
* The setup script will install commands and add them to path; use
```
stats
```
* for some basic trigger statistics, and
```
optimisation
```
* for the cut gridscan.



## Initial Cuts
Looking at the signal sample we decided that events should have:
1. njet >= 2
2. nbjets >=1
3. pass some trigger

For the trigger we decided to go with MET because it has the higest selection efficiency:

* Data 	 eff :0.942729
* SqSq_Hplus_1500_300	 eff :0.982055
* SqSq_Hplus_1500_500	 eff :0.978495
* SqSq_Hplus_1000_300	 eff :0.965482
* SqSq_Hplus_1000_500	 eff :0.939626

## Background Estimation

From the challenge description we gathered that the background samples are not a viable option for background estimation. We fully trusted that statement and resorted to a data driven estimate. For simplicity reasons we chose the ABCD method in the mjj vs mtaugamma plane. When plotting the signal and two bkg samples in a 2D plane we see a great isolation of signal in high mjj and high mtaugamma region:

![alt text](https://gitlab.cern.ch/dhagan/analysischallenge2023/double_mll_sig.pdf "Sig")
![alt text](https://gitlab.cern.ch/dhagan/analysischallenge2023/double_mll_wy.pdf "Wy")
![alt text](https://gitlab.cern.ch/dhagan/analysischallenge2023/double_mll_tty.pdf "tty")


We by eye decided for signal region (D) to be mjj > 150 and mtaugamma > 150. Inverting those cuts gives regions, A,B and C for background estimation and tests. We perform correlation studies in those regions:
| Region        | Events          | Pearson correlation  | Distance correlation |
| ------------- |:-------------:  | :-----:              | :-----:              |
| A             | 900             | 0.01+-0.03           | 0.05                 |
| C             | 237             |  0.04+-0.07          | 0.08                 |
| B             | 1917            |  -0.04+-0.02         | 0.05                 |

and are satisfied with the results.

Next we test for signal contamination and gauge at significance:
| Sample                        | A          | B       | C     | D (data is estimated with (B*C)/A) |
| -------------                 |:--------:  | :-----: | :---: | :-------: |
| Bkg                           |  900       | 1917    | 237   |  504.81   |
| Sig  1500 GeV 300 GeV         |  0.007     |  1.610  | 0.060 |  22.0388  |
| Sig  1500 GeV 500 GeV         |  0.0       |  0.884  | 0.046 |  22.5978  |
| Sig  1000 GeV 300 GeV         |  0.361     |  40.593 | 8.715 |  496.78   |
| Sig  1000 GeV 500 GeV         |  0.287     |  18.287 | 7.548 |  480.188  |

We are pretty happy with this, but for some fun we optimise with additional cuts to get higest significance with signal contamination in region C < 0.1. We plotted data vs signal for every single variable in the ntuples in region C, then chose a few variables to play with:

| Variable   | Low        | High    |
| ---------  |:--------:  | :-----: |
| bjet1pT    |  50        | 200     | 
| HT         |  400       |  800    | 
| jet1pT     |  100       |  300    | 
| jet2M      |  10        |  25     | 
| MET        |  200       |  600    | 


Our optimisation yields the following regions:

| Sample                        | Cuts      |
| ---------                     |:--------: |
| Sig  1500 GeV 300 GeV         |  /  |
| Sig  1500 GeV 500 GeV         |  / |
| Sig  1000 GeV 300 GeV         | "jet2M>=10 && jet1pT>=100 && bjet1pT>=50 && MET<600 && HT>=400 &&"  |
| Sig  1000 GeV 500 GeV         | "jet2M>=10 && jet1pT>=100 && bjet1pT>=50 && MET<600 && HT>=400 &&" |


## Uncertainties

We assign the estimated background yield a statistical uncertainty coming from the propagated error in background estimation.
The signal samples get a statistical error in the size of the sum of weights squared.
Additionally for signal we have no idea how to handle the theory systematics and EWC scales, so we generously assign 10% :grin:

Get the final yields with running:
```
root -l -b -q scripts/bkgEst.cxx
```

then in a fresh terminal:

```
git clone https://gitlab.cern.ch/HistFitter/HistFitter.git
cd HistFitter/
git checkout -b v1.0.0 v1.0.0
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.22.00-python3-x86_64-centos7-gcc8-opt"
source setup.sh
cd src/
make
cd ..

mkdir output output/inHF output/outHF output/logs
```

copy over the runExclusionFit.py, exclusionFitConf.py and change the values in the lists (the order is : data+err,sig+err with comma separation)
run `python runExclusionFit.py` and that's it!

Best results:

| Sample             | A             | B        | C          | pred D + err            | s/sqrt(b) | CLs       |  actual   D + err |
| -------------      |:-----------:  | :-----:  | :-----:    |:---------------------:  | :-----:   | :-----:   | :---------------: | 
|Bkg                 |    900        |   1917   |  237       |  504.81 +- 38.6177      |    /      | /         |   708 +- 26.6083  |
|SqSq_Hplus_1500_300 |    0.00709681 | 1.61009  |  0.0602453 |  22.0388 +- 0.0475749   | 0.9809  | 0.691606    |   /               | 
|SqSq_Hplus_1500_500 |    0          | 0.884118 |  0.0462062 |  22.5978 +- 0.0483021   | 1.00578 | 0.684159    |  /                |
|SqSq_Hplus_1000_300 |    0.361119   | 40.5931  |  8.71482   |  496.78 +-  28.1131     | 22.1106 | 2.11539e-16 |  /                |
|SqSq_Hplus_1000_500 |    0.287154   | 18.2865  |  7.54816   |  480.188 +-  26.8339    | 21.3721 | 1.54827e-15 |  /                |  



