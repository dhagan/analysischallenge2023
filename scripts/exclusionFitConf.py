from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from copy import deepcopy
from optparse import OptionParser
import sys,os 
from ROOT import gROOT, TLegend, TLegendEntry, TCanvas, TColor
import ROOT, argparse

#======================================================
#
#               Parse arguments
#
#=====================================================
inputParser = OptionParser()
inputParser.add_option('', '--RunToys',  dest = 'RunToys', default = '', help='Run frequentist exclusion fit, specifying number of toys.')
inputParser.add_option('', '--inNums',  dest = 'inNums', default = '', help='File that contains signal, bkg and observed number of events.')

configArgs = []
userArgs = configMgr.userArg.split(' ')
for arg in userArgs:
    configArgs.append(arg)
    print(arg)

(options, args) = inputParser.parse_args(configArgs)

#-------------------------------
# Parameters for hypothesis test
#-------------------------------

configMgr.fixSigXSec=True  # also does the hypotest for the up and down variations, and appends up(down) to the output filename
configMgr.testStatType=3   #3=one-sided profile likelihood test statistics, default LHC
configMgr.nPoints=20       #number of values scanned of signal-strength for upper-limit determination of signal strength.
if options.RunToys:
    print('Will run exclusion fit hypothesis tests using {0} toys.'.format(options.RunToys))
    configMgr.calculatorType=0 #0=frequentist,2=asymptotic calculator (creates asimov data set for the background hypothesis)
    configMgr.nTOYs=int(options.RunToys)
else:
    print('Will run exclusion fit hypothesis tests using asimov data set')
    configMgr.calculatorType=2 #0=frequentist,2=asymptotic calculator (creates asimov data set for the background hypothesis)

#-------------------------------
# Scaling outputLumi / inputLumi
#-------------------------------

configMgr.inputLumi = 1.0  # MC events are already weighted to lumi
configMgr.outputLumi = 1.0
configMgr.setLumiUnits("fb-1")
lumiError = 0.017

#------------------------------------------------------
# Get input numbers and name output files
#------------------------------------------------------
inNumbers = options.inNums
inNumsSplit = inNumbers.split(',')
sig_name = inNumsSplit[0]
region = "SR"

configMgr.analysisName = "susychallenge_"+region+"_"+sig_name
configMgr.histCacheFile = "output/inHF/output_"+configMgr.analysisName+".root"
configMgr.outputFileName = "output/outHF/output_"+configMgr.analysisName+"_Output.root"

nsig_D = float(inNumsSplit[3])
sigSRErr_D = float(inNumsSplit[4])

nbkg_D = float(inNumsSplit[1])
bkgUncert_D = float(inNumsSplit[2])

#======================================================
#
#                  Exclusion fit
#
#======================================================
    
configMgr.doExclusion=True    
    
# Define cuts, events have already been through the cut selections so just set to 1
configMgr.cutsDict[region] = "1."

# Define weights, already weighted
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
bkgSample.buildHisto([nbkg_D],region,"cuts",0.5)
bkgSample.buildStatErrors([bkgUncert_D],region,"cuts")

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto([nbkg_D],region,"cuts",0.5)

sigSample = Sample(sig_name,kPink)
sigSample.setNormFactor("mu_Sig",1.,0.,5.0)
sigSample.setStatConfig(True) #This sample gets statistical uncertainties
sigSample.buildHisto([nsig_D],region,"cuts",0.5)
sigSample.buildStatErrors([sigSRErr_D],region,"cuts")
sigSample.setNormByTheory() #uncertainties due to the luminosity are added
    
theoSysSig = Systematic("SigXSec", configMgr.weights, 1.2, 0.8, "user","userOverallSys")
sigSample.addSystematic(theoSysSig)

# Define top-level                                    
ana = configMgr.addFitConfig("SPlusB_%s"%sig_name)
ana.addSamples([bkgSample,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define measurement                              
meas = ana.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_Sig")
    
# Add the channel
chan = ana.addChannel("cuts",[region],1,0.5,1.5)
ana.addSignalChannels([chan])
