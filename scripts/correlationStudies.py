import ROOT
from math import sqrt, fabs
from ROOT import TFile
import ast
import os.path
import warnings
import dcor
import numpy as np
import scipy

def getCorrelation(file_tree):

    mjj_CR1 = []
    mtg_CR1 = []

    mjj_CR2 = []
    mtg_CR2 = []

    mjj_CR3 = []
    mtg_CR3 = []

    for evt in file_tree:
        if not (evt.njet >= 2 and evt.nbjet >= 1 and evt.passTrig_MET == 1): continue
       	if evt.mjj < 150   and evt.mtaugamma < 150:
           mjj_CR1.append(evt.mjj)
           mtg_CR1.append(evt.mtaugamma)
        if evt.mjj < 150   and evt.mtaugamma > 150:
  	       	mjj_CR2.append(evt.mjj)
  	       	mtg_CR2.append(evt.mtaugamma)

        if evt.mjj > 150   and evt.mtaugamma < 150:
  	       	mjj_CR3.append(evt.mjj)
  	       	mtg_CR3.append(evt.mtaugamma)

    n_CR1 = len(mjj_CR1)
    n_CR2 = len(mjj_CR2)
    n_CR3 = len(mjj_CR3)

    print("N events in CR1: {}, CR2: {}, CR3: {}".format(n_CR1, n_CR2, n_CR3))
    if ((n_CR1 < 2) or (n_CR2 < 2) or (n_CR3 < 2)): return

    dcor_CR1 = dcor.distance_correlation(np.array(mjj_CR1),np.array(mtg_CR1))
    dcor_CR2 = dcor.distance_correlation(np.array(mjj_CR2),np.array(mtg_CR2))
    dcor_CR3 = dcor.distance_correlation(np.array(mjj_CR3),np.array(mtg_CR3))

    p_CR1 = scipy.stats.pearsonr(np.array(mjj_CR1),np.array(mtg_CR1))[0]
    p_CR1_err = sqrt((1-pow(p_CR1,2))/(len(mjj_CR1)-2))
    p_CR2 = scipy.stats.pearsonr(np.array(mjj_CR2),np.array(mtg_CR2))[0]
    p_CR2_err = sqrt((1-pow(p_CR2,2))/(len(mjj_CR2)-2))
    p_CR3 = scipy.stats.pearsonr(np.array(mjj_CR3),np.array(mtg_CR3))[0]
    p_CR3_err = sqrt((1-pow(p_CR3,2))/(len(mjj_CR3)-2))

    print("CR1 & {} & {:.2f}+-{:.2f} & {:.2f}  \\\\".format(n_CR1,p_CR1,p_CR1_err,dcor_CR1))
    print("CR2 & {} & {:.2f}+-{:.2f} & {:.2f}  \\\\".format(n_CR2,p_CR2,p_CR2_err,dcor_CR2))
    print("CR3 & {} & {:.2f}+-{:.2f} & {:.2f}  ".format(n_CR3,p_CR3,p_CR3_err,dcor_CR3))

ntuple_dir = '~/Work/susychallenge/files/'

files = ['data','Wy','tty','SqSq_Hplus_1000_300','SqSq_Hplus_1000_500','SqSq_Hplus_1500_300','SqSq_Hplus_1500_500']

for file in files:

  file_name = ntuple_dir+file+".root"
  bkg_file = TFile(file_name)
  bkg_tree = bkg_file.Get("ntuple_THEORY")
  print("Doing file {}".format(file))
  getCorrelation(bkg_tree)

