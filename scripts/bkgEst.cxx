
void bkgEst(){



  TChain *chain4 = new TChain;
  chain4->Add("/home/atlas/nribaric/Work/susychallenge/files/data.root/ntuple_THEORY");
  ROOT::RDataFrame df_data(*chain4);

  //std::string cuts = "nbjet >= 1 && njet >= 2 && MET > 250 && gamma1pT > 20 && tau1pT > 20 && passTrig_MET == 1 && HT > 400 &&";
  std::string cuts = "nbjet >= 1 && njet >= 2 && passTrig_MET == 1 &&";

  std::string regA = "mjj < 150 && mtaugamma < 150";
  std::string regB = "mjj > 150 && mtaugamma < 150";  
  std::string regC = "mjj < 150 && mtaugamma > 150";
  std::string regD = "mjj > 150 && mtaugamma > 150";

  std::vector<std::string> regs = {"jet2M>=10 && jet1pT>=100 && bjet1pT>=50 && MET<600 && HT>=400 &&",""};

  for(auto reg : regs){

    float A = *df_data.Filter(cuts+reg+regA).Count();
    float B = *df_data.Filter(cuts+reg+regB).Count();
    float C = *df_data.Filter(cuts+reg+regC).Count();
    float Aerr = sqrt(A);
    float Berr = sqrt(B);
    float Cerr = sqrt(C);

    float Dbkg = (B*C)/A;

    float Derr = Dbkg * std::pow(
    std::pow( Aerr/A ,2 ) +
    std::pow( Berr/B ,2 ) +
    std::pow( Cerr/C ,2 )
    ,0.5);

    std::cout << "Bkg est A,B,C and D:   " << A << "   " << B << "   " << C << "   " << Dbkg << " " << Derr << std::endl;
    std::vector<std::string> sigs = {"SqSq_Hplus_1500_300","SqSq_Hplus_1500_500","SqSq_Hplus_1000_300","SqSq_Hplus_1000_500"};

    for (auto sig : sigs){

      float Dsig = 0;
      float Asig = 0;
      float Bsig = 0;
      float Csig = 0;

      float Dsigerr = 0;
      
  
      std::string sig_name = "/home/atlas/nribaric/Work/susychallenge/files/"+sig+".root/ntuple_THEORY";
      TChain *chain5 = new TChain;
      chain5->Add(sig_name.c_str());
      ROOT::RDataFrame df_sig(*chain5);

      df_sig.Filter(cuts+reg+regA).Foreach([&](float weight){Asig +=weight*140;}, {"EventWeight"});
      df_sig.Filter(cuts+reg+regB).Foreach([&](float weight){Bsig +=weight*140;}, {"EventWeight"});
      df_sig.Filter(cuts+reg+regC).Foreach([&](float weight){Csig +=weight*140;}, {"EventWeight"});
      df_sig.Filter(cuts+reg+regD).Foreach([&](float weight){Dsig +=weight*140;}, {"EventWeight"});
      df_sig.Filter(cuts+reg+regD).Foreach([&](float weight){Dsigerr +=(weight*140)*(weight*140);}, {"EventWeight"});
      
      float signif = Dsig / (sqrt(Dbkg));

      std::cout << sig << " A,B,C and D:    " << Asig << "   " << Bsig << "   " << Csig << "   " << Dsig << " " << Dsigerr << " " << signif << std::endl;
    }
  } 
}
