
TH1D * getPlot(ROOT::RDF::RNode df,std::string var){


 auto histPlus = df.Histo1D(var);

 TH1D hist = histPlus.GetValue();
 TH1D * hist_ptr = &hist;
 TH1D* hist_clone = (TH1D*)hist_ptr->Clone("");

 return hist_clone;
}


void plotVars(){


  TChain *chain4 = new TChain;
  chain4->Add("/home/atlas/nribaric/Work/susychallenge/files/data.root/ntuple_THEORY");
  ROOT::RDataFrame df_data(*chain4);

  TChain *chain3 = new TChain;
  chain3->Add("/home/atlas/nribaric/Work/susychallenge/files/SqSq_Hplus_1000_300.root/ntuple_THEORY");
  ROOT::RDataFrame df_sig(*chain3);


  TCanvas *c = new TCanvas("c","");
  c->SetCanvasSize(800, 600); //width,height
  c->SetWindowSize(850, 650);
  gPad->SetLogy();
  gStyle->SetOptStat(0);  

  for(auto key : df_data.GetColumnNames()){

    //if (key != "nbjet"){ continue;}

    std::string cuts = "mjj < 150 && mtaugamma > 150";

    TH1D * d_hist = getPlot(df_data.Filter(cuts),key);
    TH1D * sig_hist = getPlot(df_sig.Filter(cuts),key);

    sig_hist->SetMarkerColor(kBlack);
    sig_hist->SetLineColor(kBlack);
    sig_hist->SetMarkerStyle(8);
    sig_hist->SetMarkerSize(1.2);

    d_hist->Draw("eX0");
    sig_hist->Draw("eX0 sames");

    TAxis * yAxis =  d_hist->GetYaxis();
    yAxis->SetTitle("Entries [logy]");
    yAxis->SetTitleOffset(1.2);
    yAxis->SetLabelFont(42);

    TAxis * xAxis =  d_hist->GetXaxis();
    xAxis->SetTitle(key.c_str());
    xAxis->SetTitleOffset(1.2);
    xAxis->SetLabelFont(42);

    auto legend = new TLegend(0.7,0.75,0.92,0.92);//x1,y1,x2,y2
    legend->SetBorderSize(0);
    legend->AddEntry(d_hist, "data", "P");
    legend->AddEntry(sig_hist, "sig", "P");
    legend->Draw();

    c->SaveAs(("./hists/"+key+"_bkg_signal_comparison_regC.pdf").c_str());
    c->Clear();   


  }

}
