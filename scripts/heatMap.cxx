
TH1D * getPlot(ROOT::RDF::RNode df,std::string var){


 auto histPlus = df.Histo1D(var);

 TH1D hist = histPlus.GetValue();
 TH1D * hist_ptr = &hist;
 TH1D* hist_clone = (TH1D*)hist_ptr->Clone("");

 return hist_clone;
}


void heatMap(){


  TChain *chain1 = new TChain;
  chain1->Add("/home/atlas/nribaric/Work/susychallenge/files/tty.root/ntuple_THEORY");
  ROOT::RDataFrame df_tt(*chain1);

  TChain *chain2 = new TChain;
  chain2->Add("/home/atlas/nribaric/Work/susychallenge/files/Wy.root/ntuple_THEORY");
  ROOT::RDataFrame df_w(*chain2);

  TChain *chain3 = new TChain;
  chain3->Add("/home/atlas/nribaric/Work/susychallenge/files/SqSq_Hplus_1000_300.root/ntuple_THEORY");
  ROOT::RDataFrame df_sig(*chain3);

  TChain *chain4 = new TChain;
  chain4->Add("/home/atlas/nribaric/Work/susychallenge/files/SqSq_Hplus_1500_300.root/ntuple_THEORY");
  ROOT::RDataFrame df_sig2(*chain4);
  

  TCanvas *c = new TCanvas("c","");
  c->SetCanvasSize(800, 600); //width,height
  c->SetWindowSize(850, 650);

  gStyle->SetOptStat(0);  
  gStyle->SetPalette(kGreyScale);
  TColor::InvertPalette();

  auto hist = df_sig2.Histo2D({"","",100,0,1000,100,0,1000},"mtaugamma", "mjj");

  hist->GetXaxis()->SetTitle("taugamma");
  hist->GetYaxis()->SetTitle("mjj");
  hist->DrawClone("COLZ");
  
  c->SaveAs("./double_mll_sig2.pdf");

}
