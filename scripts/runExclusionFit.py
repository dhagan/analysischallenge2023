#!/usr/bin/env python
import os

signals = ["SqSq_Hplus_1500_300","SqSq_Hplus_1500_500","SqSq_Hplus_1000_300","SqSq_Hplus_1000_500"]
yields = ["504.81,38.6177,22.0388,0.0475749","504.81,38.6177,22.5978,0.0483021","504.81,38.6177,496.78,28.1131","504.81,38.6177,480.188,26.8339"]

for i,sig in enumerate(signals):
  name = sig
  nums = sig+","+yields[i]
  print(nums)
  os.system('HistFitter.py -u " --inNums ' + nums +' " -t -wp -f -D "allPlots" -F excl -g '+sig+' exclusionFitConf.py > output/logs/'+sig+'_log.txt')

